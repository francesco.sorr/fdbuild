#!/usr/bin/env python3

# ---------------------------------------------------------------------
# FDBuild
# Copyright © 2020 Roman Gilg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

import multiprocessing
import core.stage as stage
import plugins.stages.configure

class Plugin(stage.Stage):
    def __init__(self, args):
        super().__init__('build', args[0])
        self.directory = self.identifier

        self.step.add_caller('configure')
        self.step.add_dependent('install')

    def default_host(self):
        return plugins.stages.configure.Plugin.default_host()


    def read_threads(self):
        """Returns integer number of threads if found in settings for building or if not defined None.
        Threads can be defined in settings as whole numbers or as percentage. Additionally settings
        can specify 'max' or 'unlimited' to always select the maximum number of cores."""
        threads = self.step.read_setting('threads')
        if not threads:
            return

        if isinstance(threads, int):
            return threads

        if not isinstance(threads, str):
            return

        threads.strip()
        cpu_count = multiprocessing.cpu_count()
        if threads.lower() in ['max', 'unlimited']:
            return cpu_count

        if threads[-1] == '%':
            number = threads[:-1]
            if number.isnumeric():
                number = int(number)
                if number > 0 and number / 100 < 1:
                    return max(int(cpu_count * number / 100), 1)


    def build_path(self):
        subpath = self.step.read_setting('path', default = 'build')
        return self.step.unit.absolute_path(subpath)
