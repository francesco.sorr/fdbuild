#!/usr/bin/env python3

# ---------------------------------------------------------------------
# FDBuild
# Copyright © 2020 Roman Gilg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

import core.stage as stage

class Plugin(stage.Stage):
    def __init__(self, args):
        super().__init__('install', args[0])
        self.directory = self.identifier

        self.step.add_caller('build')


    def host_suffix(self):
        """Returns host-suffix setting which specifies if the installation should go into a
        separate folder naming the host target."""
        return self.step.read_setting('host-suffix')

    def sudo(self):
        """Returns if the installation should be executed as root."""
        return self.step.read_setting('sudo')
