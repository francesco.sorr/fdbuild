#!/usr/bin/env python3

# ---------------------------------------------------------------------
# FDBuild
# Copyright © 2018 Roman Gilg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

import os

import core.utils as utils
from core.exceptions import WorkError

class Plugin:
    def __init__(self, args):
        self.step = args[0]

        self.build_path = self.step.stage.build_path()

        self.hosts_queried = False
        self.hosts = []

    def query_hosts(self):
        if self.hosts_queried:
            return self.hosts
        self.hosts_queried = True

        configure_step = self.step.get_connected_step('configure', -1)
        if configure_step:
            self.hosts = configure_step.stage.read_hosts()
        else:
            self.hosts = [self.step.stage.default_host()]


    def get_progress_percentage(self, line):
        try:
            line.strip()
            if line[0] != '[':
                return None
            line = line[1:]
            part = line.split('%')[0]
            part.strip()
            return int(part)
        except:
            return None


    def work(self):
        threads = self.step.stage.read_threads()
        back_dir = os.getcwd()

        self.query_hosts()

        for host in self.hosts:
            bld_path = os.path.join(self.build_path, host['suffix'])

            utils.make_dir(bld_path)
            os.chdir(bld_path)

            cmd = ['make']
            if threads:
                cmd += ['-j' + str(threads)]

            if utils.run_process(cmd, self.step.log, self.get_progress_percentage) != 0:
                raise WorkError(self.step.log)

        os.chdir(back_dir)


    def work_dependent(self, dependent):
        back_dir = os.getcwd()

        threads = self.step.stage.read_threads()
        self.query_hosts()

        for host in self.hosts:
            bld_path = os.path.join(self.build_path, host['suffix'])
            os.chdir(bld_path)

            cmd = ['sudo'] if self.step.read_setting_from_connected('install', 'sudo', direction = 1) else []
            cmd += ['make', 'install']

            if threads:
                cmd += ['-j' + str(threads)]

            if utils.run_process(cmd, dependent.log, self.get_progress_percentage) != 0:
                raise WorkError(dependent.log)

        os.chdir(back_dir)
