#!/usr/bin/env python3

# ---------------------------------------------------------------------
# FDBuild
# Copyright © 2018 Roman Gilg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

import os, shutil

import core.config as cfg
import core.utils as utils
from core.exceptions import WorkError

class Plugin:
    def __init__(self, args):
        self.step = args[0]

    def work(self):
        src_path = self.step.stage.source_path()
        bld_path = self.step.stage.build_path()
        inst_path = self.step.stage.install_path()

        build_backend = self.step.stage.build_backend()
        opts = self.get_options_list()

        back_dir = os.getcwd()

        clear = self.step.read_setting_from_connected('build', 'clear', direction = 1)
        if clear:
            try:
                shutil.rmtree(bld_path)
            except FileNotFoundError:
                pass

        utils.make_dir(bld_path)
        os.chdir(bld_path)

        if build_backend == 'ninja':
            has_makefile = os.path.isfile('build.ninja')
            ninja_opt = ['-GNinja']
        else:
            has_makefile = os.path.isfile('Makefile')
            ninja_opt = []

        # If 'clear' is true there can't be any build files.
        assert not (clear and has_makefile)

        # Note: Independent of 'clear' and 'has_makefile' we continue.
        #       CMake is able to do that on the fly.
        cmd = ['cmake', '-DCMAKE_INSTALL_PREFIX=' + inst_path] + ninja_opt + opts + [ src_path ]
        ret = utils.run_process(cmd, self.step.log)

        os.chdir(back_dir)

        if ret != 0:
            raise WorkError(self.step.log)


    def get_options_list(self):
        cfg_opts = self.step.read_setting('options')
        if not cfg_opts:
            return []
        if not isinstance(cfg_opts, list):
            raise WorkError(msg = "CMake options not defined as list.")
        else:
            for i in range(len(cfg_opts)):
                opt = cfg_opts[i]
                if not isinstance(opt, str):
                    raise WorkError(msg = "CMake option not a string.")
                if not opt[:2] == '-D':
                    # add -D flag
                    cfg_opts[i] = '-D' + opt
        return cfg_opts
