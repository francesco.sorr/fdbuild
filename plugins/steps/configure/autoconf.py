#!/usr/bin/env python3

# ---------------------------------------------------------------------
# FDBuild
# Copyright © 2018 Roman Gilg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

import os, shutil, subprocess

import core.config as cfg
import core.utils as utils
from core.exceptions import WorkError

class Plugin:
    def __init__(self, args):
        self.step = args[0]

    def work(self):
        src_path = self.step.stage.source_path()
        bld_path_base = self.step.stage.build_path()
        inst_path_base = self.step.stage.install_path()

        install_step = self.step.get_connected_step('install', 1)
        inst_host_suffixed = install_step.stage.host_suffix()

        opts = self.step.unit.settgs.read_configure_options()
        hosts = self.step.stage.read_hosts()

        log = self.step.log

        bootstrap = None

        back_dir = os.getcwd()

        for host in hosts:
            if inst_host_suffixed:
                inst_path = os.path.join(inst_path_base, host['suffix'])
            else:
                inst_path = inst_path_base

            bld_path = os.path.join(bld_path_base, host['suffix'])
            utils.make_dir(bld_path)
            os.chdir(bld_path)

            has_makefile = os.path.isfile('Makefile')
            clear = self.step.read_setting_from_connected('build', 'clear', direction = 1)

            if has_makefile and not clear:
                continue

            if clear:
                os.chdir(back_dir)
                try:
                    shutil.rmtree(bld_path)
                except FileNotFoundError:
                    pass
                utils.make_dir(bld_path)
                os.chdir(bld_path)
            elif has_makefile:
                build_step = self.step.get_connected_step('build', 1)
                threads = build_step.stage.read_threads()
                if utils.run_process(['make clean', '-j' + str(threads)], log) != 0:
                    log.print("Can not run make clean.")

            if bootstrap is None:
                bootstrap = self.get_bootstrap(src_path)

                if bootstrap is None:
                    os.chdir(back_dir)
                    raise WorkError(log, "Could not locate bootstrap file for autotools at '%s'" %src_path)

            host_option = self.get_host_option(host)
            build_option = self.get_build_option(host)
            cfgcall = [bootstrap, '--prefix=' + inst_path] + host_option + build_option + opts

            old_pkgcfg_path = utils.read_and_set_pkgcfg_path(host)

            ret = utils.run_process(cfgcall, log)

            utils.reset_pkgcfg_path(old_pkgcfg_path)

            if ret != 0:
                raise WorkError(log)

        os.chdir(back_dir)

    def get_bootstrap(self, source_path):
        bootstrap = os.path.join(source_path, 'autogen.sh')

        if os.path.exists(bootstrap):
            return bootstrap

        bootstrap = os.path.join(source_path, 'configure')
        if os.path.exists(bootstrap):
            return bootstrap

        return None

    def get_host_option(self, host):
        if host['host'] == 'default':
            return []

        # TODO: Sometimes/always(?) PKG_CONFIG_PATH needs to be set.
        if host['host'] == 'x86':
            bits = host['bits']
            if bits == 64:
                return ['--host=x86_64-linux-gnu', 'CFLAGS=-m64', 'CXXFLAGS=-m64', 'LDFLAGS=-m64']
            if bits == 32:
                return ['--host=i686-linux-gnu', 'CFLAGS=-m32', 'CXXFLAGS=-m32', 'LDFLAGS=-m32']

        return []

    def get_build_option(self, host):
        if host['host'] == 'default':
            return []

        if host['host'] == 'x86':
            bits = host['bits']
            if bits == 64:
                return ['--build=x86_64-pc-linux-gnu']
            if bits == 32:
                return ['--build=i686-linux-gnu']
        return []
