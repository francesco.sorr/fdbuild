#!/usr/bin/env python3

# ---------------------------------------------------------------------
# FDBuild
# Copyright © 2018 Roman Gilg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

def projectsettings():
    return '''#
# Project settings file for FDbuild
#
# This file defines a FDBuild project in the directory it is
# located in. Its settings are applied to subprojects, but
# they can overwrite specific values in their own setting files,
# which then apply to all further sub-projects and modules in
# their directory hierarchy.
#
# To reset the setting values here, delete this file. The file
# will be regenerated the next time you run FDbuild.
#

# list subprojects - uninducible
# list only '/': all subdirectories (excl. 'src' and 'build')
projects:
    -

envvars:
    # environment variables to be set while working on this project
    #
    #VAR_NAME:
        #value: VAR_VALUE
        #prepend: false # prepend instead of replace - optional (default: false)

source:
    # url to pull source updates from - uninducible
    origin:

configure:
    options:

build:
    threads:

install:
    path:
    sudo: false

plugins:
    structure:
    source:
    configure:
    build:
'''
