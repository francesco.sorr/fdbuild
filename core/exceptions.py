#!/usr/bin/env python3

# ---------------------------------------------------------------------
# FDBuild
# Copyright © 2019 Roman Gilg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

class InitError(Exception):
    """Raised on a generic error when initializing the units"""
    def __init__(self, log = None, msg = None):
        super(InitError, self).__init__(msg)

        self.message = msg
        self.log = log

class TemplateError(Exception):
    """Raised on a generic error when creating templating a project"""
    def __init__(self, log = None, msg = None):
        super(TemplateError, self).__init__(msg)

        self.message = msg
        self.log = log


class WorkError(Exception):
    """Raised on a generic error when working"""
    def __init__(self, log = None, msg = None):
        if msg is None:
            # Set some default useful error message
            msg = "A work error occured"
        super(WorkError, self).__init__(msg)

        self.log = log

    def stdout(self):
        return self.log.stdout if self.log else ''
    def stderr(self):
        return self.log.stderr if self.log else ''


class StructurizeError(Exception):
    """Raised on a generic error when creating the structure"""
    def __init__(self, plugin_name = 'unknown', log = None, msg = None):
        if msg is None:
            # Set some default useful error message
            msg = "A structure error occured"
        super(StructurizeError, self).__init__(msg)

        self.plugin_name = plugin_name
        self.log = log


class HookError(WorkError):
    """Raised when executing a hook failed"""
    def __init__(self, post, log = None, msg = None):
        self.post = post
        super().__init__(log, msg)
