# FDBuild Tooling
## Documentation
Uses the commit message lint functionality from the [tooling repo][kwinft-tooling].
See there for information on how the commitlint tool can be run locally too.

[kwinft-tooling]: https://gitlab.com/kwinft/tooling
